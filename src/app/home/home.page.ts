import {Component, ViewChild, OnInit} from '@angular/core';
import {ModalController, ToastController} from '@ionic/angular';
import {QrScannerComponent} from '../modals/qr-scanner/qr-scanner.component';
import {ParamsService} from '../services/params.service';
import {Router} from '@angular/router';
import {ZXingScannerComponent} from '@zxing/ngx-scanner';
import {BarcodeFormat} from '@zxing/library';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

    leido = false;
    @ViewChild('scanner', {static: false}) scanner: ZXingScannerComponent;
    allowedFormats = [BarcodeFormat.QR_CODE];
    camaras;

    constructor(private modalCtrl: ModalController,
                private toastCtrl: ToastController,
                public params: ParamsService,
                private router: Router
    ) {
    }

    ngOnInit(): void {
    }

    async leerQR() {
        const modal = await this.modalCtrl.create({
            component: QrScannerComponent
        });
        await modal.present();

        const {data} = await modal.onDidDismiss();
        console.log(data);
        if (data) {
            this.params.set(JSON.parse(atob(data)));
            this.router.navigateByUrl('/info');
        }
    }

    async showToast(message, duration) {
        const toast = await this.toastCtrl.create({
            message,
            position: 'top',
            duration
        });
        toast.present();
    }

    scanError(e) {
        console.log('error:', e);
    }

    ionViewWillEnter() {
        this.leido = false;
    }

    async scanSuccess(e) {
        console.log('success:', e);
        if (e) {
            this.leido = true;
            this.params.set(JSON.parse(atob(e)));
            this.router.navigateByUrl('/info');
            // await this.modal.dismiss(e).finally();
        }
    }

    scanComplete(e) {
        if (e) {
            this.scanner.enable = false;
            console.log('complete:', e);
        }
    }

    camerasFoundHandler(e) {
        this.camaras = e;
    }

    async dvChange(e) {
        console.log('change: ', e);
        const back = await this.camaras.findIndex(asd => asd.label.includes('front'));
        console.log('back: ', back);
        if (back >= 0) {
            delete this.camaras[back];
            this.scanner.device = this.camaras[0];
        } else {
            this.scanner.device = this.camaras[0];
        }
    }

    ionViewWillLeave() {
        console.log('se fue de la pag');
    }
}
