import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ParamsService} from '../services/params.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {
  public data;
  constructor(private router: Router, private params: ParamsService) {
    this.data = params.get();
    console.log(this.data);
  }

  ngOnInit() {
  }

  solicitud() {
    this.router.navigateByUrl('/login');
  }
}
