import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ParamsService {
  private data: any;
  constructor() { }
  set = (obj: any) => {
    this.data = obj;
  };
  get = () => {
    const tmp = this.data;
    this.data = undefined;
    return tmp;
  }
}
