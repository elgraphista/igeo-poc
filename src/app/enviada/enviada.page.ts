import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-enviada',
  templateUrl: './enviada.page.html',
  styleUrls: ['./enviada.page.scss'],
})
export class EnviadaPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }


  solicitud() {
    this.router.navigateByUrl('/solicitud');
  }
  scan() {
    this.router.navigate(['/home']);
  }
}
