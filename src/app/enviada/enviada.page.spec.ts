import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnviadaPage } from './enviada.page';

describe('EnviadaPage', () => {
  let component: EnviadaPage;
  let fixture: ComponentFixture<EnviadaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnviadaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnviadaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
